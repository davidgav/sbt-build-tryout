package secondModuleCore

object Core {
    def helloCore: String = s"this is ${secondModuleEnv.Env.getEnv}!"

    def helloModuleCore: String = s"module says hi:\n${moduleCore.Core.helloCore}\nSpark version is: ${moduleEnv.Env.sparkVersion}"
}
