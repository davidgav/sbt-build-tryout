name := "solution-4"
version := "0.1"
scalaVersion := "2.11.12"

val moduleBase =
    Def.setting((Compile / scalaSource).value.getParentFile.getParentFile.getParentFile)

def moduleName(base: String, axis: Seq[VirtualAxis]): String = {
    val series = axis.collectFirst { case c: envAxis => c.series }.get
    s"${base}${series}"
}

//lazy val wasp = (project in file("."))
//    .settings(
//        // dependencies etc.
//    )
//    .dependsOn(secondModule.projectRefs(1))
// // uncoment code above if you want to run root of this project (code in solution-4/src/main/scala/)

lazy val wasp = (projectMatrix in file("."))
    .aggregate(module, secondModule)

lazy val module = (projectMatrix in file("module"))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env1), identity(_))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env2), identity(_))
    .settings(
        name := moduleName("module", virtualAxes.value),
        libraryDependencies ++= {
            val env: String = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            val versions = env match {
                case "env1" =>
                    Versions_env1
                case "env2" =>
                    Versions_env2
                case _ =>
                    Versions_env1
            }
            Dependencies.getDependenciesForEnv(versions)
        },
        Compile / unmanagedSourceDirectories += {
            val env = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            env match {
                case "env1" =>
                    moduleBase.value / "src" / "main" / "scala-env1"
                case "env2" =>
                    moduleBase.value / "src" / "main" / "scala-env2"
                case _ => // env1 is default (can be changed of course)
                    moduleBase.value / "src" / "main" / "scala-env1"
            }
        }
    )

lazy val secondModule = (projectMatrix in file("secondModule"))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env1), identity(_))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env2), identity(_))
    .settings(
        name := moduleName("secondModule", virtualAxes.value),
        Compile / unmanagedSourceDirectories += {
            val env = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            env match {
                case "env1" =>
                    moduleBase.value / "src" / "main" / "scala-env1"
                case "env2" =>
                    moduleBase.value / "src" / "main" / "scala-env2"
                case _ => // env1 is default (can be changed of course)
                    moduleBase.value / "src" / "main" / "scala-env1"
            }
        }
    )
    .dependsOn(module)

