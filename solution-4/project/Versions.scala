trait Versions {
    val scala: String = "2.11.12"
    val spark: String
}

object Versions_env1 extends Versions {
    override val spark = "2.0.0"
}

object Versions_env2 extends Versions {
    override val spark = "2.0.1"
}