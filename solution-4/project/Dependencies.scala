import sbt._

object Dependencies {
    def getDependenciesForEnv(versions: Versions): Seq[ModuleID] = {
        Seq(
            "org.apache.spark" %% "spark-core" % versions.spark,
            "org.apache.spark" %% "spark-sql" % versions.spark
        )
    }
}

