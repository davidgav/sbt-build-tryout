import sbt.VirtualAxis

case class envAxis(series: String, environment: String) extends VirtualAxis.WeakAxis {
    val idSuffix: String = series
    val directorySuffix: String = series
}

object envAxis {
    lazy val env1: envAxis = envAxis("-env1", "env1")
    lazy val env2: envAxis = envAxis("-env2", "env2")
}
