## [Solution 4] Sbt project matrix https://github.com/sbt/sbt-projectmatrix

solution 2 and this are based on the same principle with the difference being that solution 2 uses sbt without plugins to build the project, while in this solution sbt project matrix plugin is used.

This is a doc about what project matrix is (not sbt project matrix, but project matrix in general):
https://xtuml.org/wp-content/uploads/2015/10/The-Project-Matrix.pdf

In this example there will be two modules and project's file structure will look something like this:
```
|solution-4
    |-build.sbt
    |-project/
    |   |-build.properties
    |   |-Dependencies.scala
    |   |-Versions.scala
    |
    |-module1/
    |   |-src/
    |       |-main/
    |           |-scala
    |           |-scala-env1
    |           |-scala-env2
    |
    |-module2/
    |   |-src/
    |       |-main/
    |           |-scala
    |           |-scala-env1
    |           |-scala-env2
```

The point of using sbt project matrix is that we can build multiple projects (with different sources and dependencies) in parallel.

First we define versions and dependencies that different environments will use:
```
trait Versions {
    val scala: String = "2.11.12"
    val spark: String
}

object Versions_env1 extends Versions {
    override val spark = "2.0.0"
}

object Versions_env2 extends Versions {
    override val spark = "2.0.1"
}

object Dependencies {
    def getDependenciesForEnv(versions: Versions): Seq[ModuleID] = {
        Seq(
            "org.apache.spark" %% "spark-core" % versions.spark,
            "org.apache.spark" %% "spark-sql" % versions.spark
        )
    }
}
```

The next task it to allow parallel cross building. To do that, we need to add rows to our project matrix, where one row is one environment specific project. And then for different rows we should use different depencencies. Let's start with defining custom axies for environments.

In project/envAxis.scala
```
import sbt.VirtualAxis

case class envAxis(series: String, environment: String) extends VirtualAxis.WeakAxis {
    val idSuffix: String = series
    val directorySuffix: String = series
}

object envAxis {
    lazy val env1: envAxis = envAxis("-env1", "env1")
    lazy val env2: envAxis = envAxis("-env2", "env2")
}
```

Now we can add that to our build.sbt. For each project we will check the value of the axis (environment) and based on that use environment specific depencendies.
```
lazy val module = (projectMatrix in file("module"))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env1), identity(_))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env2), identity(_))
    .settings(
        name := moduleName("module", virtualAxes.value),
        libraryDependencies ++= {
            val env: String = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            val versions = env match { // check environment
                case "env1" =>
                    Versions_env1
                case "env2" =>
                    Versions_env2
                case _ =>
                    Versions_env1
            }
            Dependencies.getDependenciesForEnv(versions)
        }
    )
```

After that, we should add environment specific sources. To do that, we will basically do the same thing as above, just insted of libraryDependencies we will add to Compile / unmanagedSourceDirectories. Let's check how the code from above will look like after this modification:
```
lazy val module = (projectMatrix in file("module"))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env1), identity(_))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env2), identity(_))
    .settings(
        name := moduleName("module", virtualAxes.value),
        libraryDependencies ++= {
            val env: String = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            val versions = env match {
                case "env1" =>
                    Versions_env1
                case "env2" =>
                    Versions_env2
                case _ =>
                    Versions_env1
            }
            Dependencies.getDependenciesForEnv(versions)
        },
        Compile / unmanagedSourceDirectories += {
            val env = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            env match {
                case "env1" =>
                    moduleBase.value / "src" / "main" / "scala-env1"
                case "env2" =>
                    moduleBase.value / "src" / "main" / "scala-env2"
                case _ => // env1 is default (can be changed of course)
                    moduleBase.value / "src" / "main" / "scala-env1"
            }
        }
    )
```

Let's just create another module, this one will only have environment specific sources, and it will depend on module from above.
```
lazy val secondModule = (projectMatrix in file("secondModule"))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env1), identity(_))
    .customRow(scalaVersions = Seq("2.11.12"), axisValues = Seq(VirtualAxis.jvm, envAxis.env2), identity(_))
    .settings(
        name := moduleName("secondModule", virtualAxes.value),
        Compile / unmanagedSourceDirectories += {
            val env = virtualAxes.value.collectFirst { case c: envAxis => c.environment }.get
            env match {
                case "env1" =>
                    moduleBase.value / "src" / "main" / "scala-env1"
                case "env2" =>
                    moduleBase.value / "src" / "main" / "scala-env2"
                case _ => // env1 is default (can be changed of course)
                    moduleBase.value / "src" / "main" / "scala-env1"
            }
        }
    )
    .dependsOn(module) // <- packages from module can be used in secondModule
```

In the end, we should aggregate projects.
```
lazy val wasp = (projectMatrix in file("."))
    .aggregate(module, secondModule)
```

So now we can build our environment specific projects in parallel. After build, type sbt projects to get list of the projects. In our sample this should be the output:
```
> projects
[info] In file:/Users/davidgavrilovic/IdeaProjects/solution-4/
[info] 	   module-env12_11
[info] 	   module-env22_11
[info] 	   secondModule-env12_11
[info] 	   secondModule-env22_11
[info] 	   * solution-4
[IJ]
```
Each of the project matrices we defined in build.sbt will have one project for every row. In this case, module-env12_11 is project called module-env1 that uses scala 2.11. Here if we want to run the project in environemnt 1 we will use module-env12_11 and secondModule=env12_11.

Position into the project you want to compile and run with project command.
```
> project secondModule-env12_11
[info] set current project to secondModule-env1 (in build file:/Users/davidgavrilovic/IdeaProjects/solution-4/)
[IJ]
```

After that you can compile it and run it.

This solution is good and it is the only one that allows us to build environment specific code in **parallel** while allowing the flexibilities of other solutions.


Some resources on this that might be usefull:
- [sbt project matrix on github](https://github.com/sbt/sbt-projectmatrix)
- [Blog post about parallel cross building]( https://eed3si9n.com/parallel-cross-building-with-virtualaxis)
- [build.sbt of the project that is built with sbt project matrix](https://github.com/jbwheatley/pact4s/blob/main/build.sbt)

