# sbt-build-tryout
--------------------------

# Problem statement

Currently we cannot build wasp with different sets of dependencies, different projects have different runtime environment, its difficult to move forward with all downstream projects at once 

# Proposed solution

* [Solution 1] Investigate multiple branches for multiple versions
* [Solution 2] Different sbt builds for each version
* [Solution 3] Same sbt build with a switch inside the build definition (multiple runs with different sets of dependencies)
* [Solution 4] Sbt project matrix https://github.com/sbt/sbt-projectmatrix
--------------------------
**Each proposed solution will have comments about it written in README.md in its respective directory**

For example, in solution-1/README.md you will find comments about solution-1

--------------------------

# Definition of done

Prototype something with the proposed solutions and decide the way to go

We should have as output Pros and Cons for each solution and decide how to move forward
