import com.typesafe.config.ConfigFactory

name := "solution-3"
version := "0.1"
scalaVersion := "2.12.12"

libraryDependencies += "com.typesafe" % "config" % "1.4.1"

val envType = ConfigFactory.parseFile(new File("src/main/resources/application.conf")).getString("some_env")

def getDependencies(envType: String): Dependencies = {
    envType match {
        case "env_1" =>
            Dependencies_env1
        case _ =>
            Dependencies_env2
    }
}

val dependencies = getDependencies(envType)

def getEnvSource(envType: String): Def.Setting[Seq[File]] = {
    envType match {
        case "env_1" =>
            Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env1"
        case _ =>
            Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env2"
    }
}

val envSource = getEnvSource(envType)

lazy val root = (project in file("."))
    .settings(name := "root")
    .aggregate(module1, module2)
    .dependsOn(module1)
    .dependsOn(module2)

lazy val module1 = (project in file("module1"))
    .settings(name := "module1")
    .settings(
        envSource
    )
    .settings(
        libraryDependencies ++= Seq(
            dependencies.akkaActor
        )
    )

lazy val module2 = (project in file("module2"))
    .settings(name := "module2")
    .settings(envSource)
    .settings(
        libraryDependencies ++= Seq(
            dependencies.sparkCore,
            dependencies.sparkSql
        )
    )



