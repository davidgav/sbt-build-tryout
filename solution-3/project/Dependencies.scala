import sbt._

trait Dependencies {
    val versions: Versions
    val sparkCore: ModuleID
    val sparkSql: ModuleID
    val akkaActor: ModuleID
}

object Dependencies_env1 extends Dependencies {
    override val versions: Versions = Versions_env1
    override val sparkCore = "org.apache.spark" %% "spark-core" % versions.spark
    override val sparkSql = "org.apache.spark" %% "spark-sql" % versions.spark
    override val akkaActor = "com.typesafe.akka" %% "akka-actor" % versions.akka
}

object Dependencies_env2 extends Dependencies {
    override val versions: Versions = Versions_env2
    override val sparkCore = "org.apache.spark" %% "spark-core" % versions.spark
    override val sparkSql = "org.apache.spark" %% "spark-sql" % versions.spark
    override val akkaActor = "com.typesafe.akka" %% "akka-actor" % versions.akka
}
