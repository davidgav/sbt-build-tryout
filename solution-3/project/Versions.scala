trait Versions {
    val spark: String
    val akka: String
}

object Versions_default extends Versions {
    override val spark: String = "3.0.0"
    override val akka: String = "2.6.10"
}

object Versions_env1 extends Versions {
    override val spark: String = "3.0.1"
    override val akka: String = "2.6.15"
}

object Versions_env2 extends Versions {
    override val spark = "3.0.2"
    override val akka = "2.6.13"
}