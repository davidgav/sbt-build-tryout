## [Solution 3] Same sbt build with a switch inside the build definition (multiple runs with different sets of dependencies)

Idea is to have some value in conf file (can be env variable, but we will use conf file) and based on that in the build file different dependencies and different source code will be used.  That means that there will be only one build file, but build will behave differently depending on value that we load from conf file (there will be switch inside build.sbt)

conf file you are loading from should have something like this
```
some_env = "env_1"
```
the value (in this case env_1) will determine how you build your project

If you built your project and want to do it again but for different environment you will have to change value in conf file and then reload/refresh sbt to see the changes

Let's first address how to use different dependencies based on envieronment we are running wasp from

In this solution build.sbt file will look something like this, just way more complex:
```
import com.typesafe.config.ConfigFactory

name := "build-tryout"
version := "0.1"
scalaVersion := "2.12.12"

libraryDependencies += "com.typesafe" % "config" % "1.4.1"
val envType = ConfigFactory.parseFile(new File("src/main/resources/application.conf")).getString("some_env")

envType match {
    case "env_1" =>
        val AkkaVersion = "2.6.15"
        libraryDependencies ++= Seq(
            "com.typesafe.akka" %% "akka-actor" % AkkaVersion
        )
    case _ =>
        val AkkaVersion = "2.6.13"
        libraryDependencies ++= Seq(
            "com.typesafe.akka" %% "akka-actor" % AkkaVersion
        )
}
```
In this example, different version of akka actor is used depending on the value from conf file

The obvious problem with this approach is code repetition because both cases are the same, the only difference is the version we are using. But that can be resolved!

Let's say we have our app running on two environments, called env_1 and env_2 and that we have two sub-projects inside our project (called root and tmpModule). Each of those sub-projects will have it's own environment specific dependencies as well as dependencies
that other sub-project do not have. In practice we will have way more than that.

First, in order to reduce code repetition, let's create dependency classes, where we will store dependencies for specific environments
```
trait Dependencies {
    val versions: Versions
    val sparkCore: ModuleID
    val sparkSql: ModuleID
    val akkaActor: ModuleID
}

class Dependencies_env1 extends Dependencies {
    override val versions: Versions = new Versions_env1
    override val sparkCore = "org.apache.spark" %% "spark-core" % versions.spark
    override val sparkSql = "org.apache.spark" %% "spark-sql" % versions.spark
    override val akkaActor = "com.typesafe.akka" %% "akka-actor" % versions.akka
}

class Dependencies_env2 extends Dependencies {
    override val versions: Versions = new Versions_env2
    override val sparkCore = "org.apache.spark" %% "spark-core" % versions.spark
    override val sparkSql = "org.apache.spark" %% "spark-sql" % versions.spark
    override val akkaActor = "com.typesafe.akka" %% "akka-actor" % versions.akka
}
```
Where we will have versions for each environment in the hierarchy like this:
```
trait Versions {
    val spark: String
    val akka: String
}

class Versions_env1 extends Versions {
    override val spark: String = "3.0.1"
    override val akka: String = "2.6.15"
}

class Versions_env2 extends Versions {
    override val spark = "3.0.2"
    override val akka = "2.6.13"
}
```
After that we can define a function that will return dependencies that are environment specific. Function will return dependencies for different values that we are reading from conf file (envType in this example)
```
def getDependencies(envType: String): Dependencies = {
    envType match {
        case "env_1" =>
        new Dependencies_env1
        case _ =>
            new Dependencies_env2
    }
}
```
Now we can create projects inside build.sbt that are using dependencies that are ENVIRONMENT specific
```
val dependencies = getDependencies(envType)

lazy val root = (project in file("."))
    .settings(name := "root")
    .settings(
        libraryDependencies ++= Seq(
            dependencies.sparkCore,
            dependencies.sparkSql
            // ...
        )
    )
    .aggregate(tmpModule)

lazy val tmpModule = (project in file("tmpModule"))
    .settings(name := "tmpModule")
    .settings(
        libraryDependencies ++= Seq(
            dependencies.akkaActor
            // ...
        )
    )
```
All of this means that if different environments have same structure of build.sbt we can eliminate code repetition. This also increase readability and maintenance because each environments will have it's own dependency class, so those will not be problem here.

The other thing we need to address in this solution is that we need to use different source code in different environments

Every module will have structure like this
module
    |-src/
        |-main
            |-scala
            |-scala-env1
            |-scala-env2

Code in directory scala directory will be used in every environment, while scala-env1 and scala-env2 will be used in environment specific settups

In sbt, by default, source will be in src/main/scala directory, but you can add additional sources. That can be done with following command:
```
Compile / unmanagedSourceDirectories += baseDirectory.value / "path/to/external/src"
```
By adding following lines in build.sbt we can add scala-env1 and scala-env2 directories to source
```
Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env1"
Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env2"
```

For this example, let's say we have and two environments, env1 and env2 and two modules (besides root module), module1 and module2 with both environments having file structure from above.

We will create a function that will select which sources to use based on the value in conf file.
```
def getEnvSource(envType: String): Def.Setting[Seq[File]] = {
    envType match {
        case "env_1" =>
            Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env1"
        case _ =>
            Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env2"
    }
}

val envSource = getEnvSource(envType)
```
Note that src/main/scala will be part of the source by default.

When we create a project in build.sbt we will add that line to settings. For example:
```
lazy val module1 = (project in file("module1"))
    .settings(name := "module1")
    .settings(
       envSource // <------ this will include environment specific source
    )
    .settings(
        libraryDependencies ++= Seq(
            dependencies.akkaActor
        )
    )
```
In total, out build.sbt in this case will look something like this:
```
import com.typesafe.config.ConfigFactory

name := "solution-3"
version := "0.1"
scalaVersion := "2.12.12"

libraryDependencies += "com.typesafe" % "config" % "1.4.1"

val envType = ConfigFactory.parseFile(new File("src/main/resources/application.conf")).getString("some_env")

def getDependencies(envType: String): Dependencies = {
    envType match {
        case "env_1" =>
            new Dependencies_env1
        case _ =>
            new Dependencies_env2
    }
}

val dependencies = getDependencies(envType)

def getEnvSource(envType: String): Def.Setting[Seq[File]] = {
    envType match {
        case "env_1" =>
            Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env1"
        case _ =>
            Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala-env2"
    }
}

val envSource = getEnvSource(envType)

lazy val root = (project in file("."))
    .settings(name := "root")
    .aggregate(module1, module2)
    .dependsOn(module1)
    .dependsOn(module2)

lazy val module1 = (project in file("module1"))
    .settings(name := "module1")
    .settings(
        envSource
    )
    .settings(
        libraryDependencies ++= Seq(
            dependencies.akkaActor
        )
    )

lazy val module2 = (project in file("module2"))
    .settings(name := "module2")
    .settings(envSource)
    .settings(
        libraryDependencies ++= Seq(
            dependencies.sparkCore,
            dependencies.sparkSql
        )
    )
```

Project with this solution can be found at: https://gitlab.com/davidgav/sbt-build-tryout/-/tree/main/solution-3

### Some comments on this solution
From different dependencies perspective this approach is extensible. If you want to add new environment you just need to add new dependency and version classes, new value in conf file and you are good to go.
From different source code per environment perspective

This approach is extensible. If you want to add new environment you need to add new versions and dependency classes, and new source code in every module where that is needed. Besides that you must add new value in conf file and update functions so they could return newly added source paths or dependencies. And you are good to go.

Build.sbt file written like this shouldn't be that hard to maintain

There might be some sneaky errors during code building. Look at this example:
```
import com.typesafe.config.ConfigFactory

name := "build-tryout"
version := "0.1"
scalaVersion := "2.12.12"

assemblyMergeStrategy in assembly := {
    case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
    case _ => MergeStrategy.first
}

mainClass in assembly := Some("Main")
libraryDependencies += "com.typesafe" % "config" % "1.4.1"
val envType = ConfigFactory.parseFile(new File("src/main/resources/application.conf")).getString("some_env")

envType match {
    case "env_1" =>
        val sparkVersion = "3.0.1"
        libraryDependencies ++= Seq(
            "org.apache.spark" %% "spark-core" % sparkVersion,
            "org.apache.spark" %% "spark-sql" % sparkVersion
        )
    case _ =>
        val sparkVersion = "2.1.3"
        libraryDependencies ++= Seq(
            "org.apache.spark" %% "spark-core" % sparkVersion,
            "org.apache.spark" %% "spark-sql" % sparkVersion
        )
}
```
Here, if the value in conf file is set to 'env_1', project will build with no problems.
**BUT** if there is some other value (let's say 'env_2' for example) project build will fail because spark 2.1.0 is not compatible with scala 2.12.12 and you will get an error
These are the things that you should be aware of when using this solution

People that work on the app will need to decide are they using env variable or value from conf file in pattern matching. And what value will be default one.

Someone will want to build a project for some environment, but they might forget to change the default value in the conf file. Because of that, build might fail and the person might have hard time finding an error.

If you want to build mulitple environments, you will have to manualy build project mulitple times, while changing value in conf file and refreshing sbt each time, and that will take some time.

