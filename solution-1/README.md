## [Solution 1] Investigate multiple branches for multiple versions

In this solution, for every environment there is a separate git branch and project will be built from there. This is similar to the case where app has different versions, and we have one branch for each one

Let's say we have environments env_1 and env_2. For each one we will have different git branch and they should have similar (but not same!) environment specific Wasp source code and different build.sbt files

usually branches are organized like this:
```
- feature/feature-name
- release/release-name
- develop
```

We can change that to this (or similar):
```
- environment/name/[feature|release|develop]
```

##### CONS:
Maintaining the code will be **hard** because each branch will have to be maintained separately and kept up to date separately

Code can be changed (like bug fix or something) and that change will need to be applied to every branch. We can do that with cherry picking

This solution will be extremely complicated if there are multiple versions of wasp, with each version having it's own branch. That means that we will have to create a branch for every environment for every version of Wasp. That means that there will be **a lot** of branches that will need to be maintained, which makes this solution even harder to work with

##### PROS:
This solution is extensible. If you want to add new environment, you can just branch of master and change build.sbt and source code and you should be good to go

To make organization and maintenance of the branches easier, every branch can have different person responsible for it. That person can approve merge requests, check if some updates to the code should be made or something similar
