Compile / unmanagedSourceDirectories += baseDirectory.value / "../module1/module1Core/src/main/scala"

libraryDependencies ++= Seq(
    CoreDependencies.akkaActor,
    EnvDependencies.sparkCore,
    EnvDependencies.sparkSql
    // add other dependencies
)