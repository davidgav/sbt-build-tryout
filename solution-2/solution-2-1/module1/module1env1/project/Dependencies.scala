import sbt._

object EnvDependencies {
    val sparkCore = "org.apache.spark" %% "spark-core" % EnvVersions.spark
    val sparkSql = "org.apache.spark" %% "spark-sql" % EnvVersions.spark
}

object EnvVersions {
    val scalaVersion = "2.12.12"
    val spark: String = "3.0.0"
}

object CoreDependencies {
    val akkaActor = "com.typesafe.akka" %% "akka-actor" % CoreVersions.akka
}

object CoreVersions {
    val akka: String = "2.6.10"
}
