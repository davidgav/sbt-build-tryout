package envModule1

import org.apache.spark.sql.SparkSession

object Env {
    def getEnv: String = "environment 2 <---"

    def sparkVersion: String = {
        val spark = SparkSession.builder().master("local").getOrCreate()
        val ver = spark.version
        spark.close()
        ver
    }
}