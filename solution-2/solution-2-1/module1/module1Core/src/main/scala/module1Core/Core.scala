package module1Core

import com.typesafe.akka.akka-actor._

object Core {
    def helloCore: String = s"${envModule1.Env.getEnv} says hi!"
}