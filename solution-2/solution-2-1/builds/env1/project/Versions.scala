object Versions {
    val scalaVersion = "2.12.12"
    val spark: String = "3.0.0"
    val akka: String = "2.6.10"
}