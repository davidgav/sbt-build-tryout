name := "solution-2-env2"
version := "0.1"
scalaVersion := Versions.scalaVersion

lazy val wasp = (project in file("."))
    .aggregate(module1Ref)
    .dependsOn(module1Ref)
    .settings(
        Compile / scalaSource := baseDirectory.value / "../../src/main/scala"
    )

lazy val module1Ref = ProjectRef(file("../../module1/module1env2"), "module1env2")