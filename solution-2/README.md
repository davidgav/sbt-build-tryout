## [Solution 2] Different sbt builds for each version

This solution will limit us to one built project per build. If you want to build mulitple projects you will have to build them separately, one by one.

The idea is that structure of the project we need to build looks like this:
```
solution-2/
    |-builds/
    |    |-env1/
    |    |   |-build.sbt
    |    |   |-project/
    |    |       |-Dependencies
    |    |       |-build.properties
    |    |
    |    |-env2/
    |        |-build.sbt
    |        |-project/
    |            |-Dependencies
    |            |-build.properties
    |     
    |-module1/
    |    |-src/
    |        |-main
    |            |-scala
    |            |-scala-env1
    |            |-scala-env2
    |
    |-module2/
        |-src/
            |-main
                |-scala
                |-scala-env1
                |-scala-env2
```
module/main/scala should be part of the project regardless of the environment while scala-env[1, 2, etc.] directories are environment specific.

This means that we should build our project from inside solution-2/builds/[env1, env2, etc.] directories. Every build.sbt file inside will have everything that we need to build project in selected environment. For example, if we are building in env1, we need to build project from solution-2/builds/env1.

The problem is, build.sbt can't create sub-projects that are **outside** of the directory build.sbt is in (outside of the root of the project), it can just reference them. That means that we will get an error when we run this code:
```
lazy val proj = (project in file("../../"))
```
We will have to use ProjectRefs to reffer to modules 1 and 2 from solution-2/builds/env directories. We end up with something like this.
```
lazy val wasp = (project in file("."))
    .aggregate(module1Ref, module2Ref)
    .dependsOn(module1Ref)
    .dependsOn(module2Ref)

lazy val module1Ref = ProjectRef(file("../../module1"), "module1")

lazy val module2Ref = ProjectRef(file("../../module2"), "module2")
```
But this approach has a big problem, and that is that we cannot set our own settings in ProjectRefs. This code throws and error:
```
lazy val module1Ref = ProjectRef(file("../../module1"), "module1")
    .settings(
        // define dependencies, src code location etc.
    )
```
That means that we cannot use this approach to define dependencies or env specific sources for modules. There is a hack that we can use here, and that is to use build.sbt for each module (sub-project) and define sources and dependencies there, but we will somehow need to choose which one to use because both of those things are environment specific... We will end up with some switch inside of those build files so this approach is kinda hybrid between solutions 2 and 3.

We cannot define environment specific sources like this either:
```
lazy val module1Ref = ProjectRef(file("../../module1"), "module1")
module1Ref / Compile / unmanagedSourceDirectories += baseDirectory.value / "../../module1/src/main/scala-env1"
```

Basically, the project that we are having a reference to will build by the rules defined in it's own build.sbt, and we cannot change or edit that.

That means that we need to do something else... but what? Because solution 2 with this project structure seems impossible to do because there is not way to add/remove dependencies from external projects (outisde of the root of the project) nor there is a way yo change it's sources, and **both** things are needed.

### Trying alternative approach (solution-2-1)

There is an idea to use solution 2, but it deviates from project structure from above and also has some drawbacks (code repetition). Every module will have it's own build for each of the environments meaning that module1 will have to be split into module1Env1, module1Env2 etc. and from builds/env we will build what we have and create references to those environment specific projects. Because of that, dependencies and version files in solution-2/builds/env[1,2,etc.]/project/ are not needed, and could be deleted. Basically, we will let modules build themselves how they want to and after that we will just use them.

In this example we will have two environments and one module, so file structure will look like this:
```
solution-2/
    |-builds/
    |    |-env1/
    |        |-build.sbt
    |        |-project/
    |            |-Dependencies
    |            |-build.properties
    |
    |    |-env2/
    |        |-build.sbt
    |        |-project/
    |            |-Dependencies
    |            |-build.properties
    |
    |-module1env1/
    |    |-build.sbt
    |    |-project
    |    |      |-Dependencies/Versions/buid.properties
    |    |
    |    |-src/
    |        |-main
    |            |-scala
    |               |-core
    |              |-envSpecificThings
    |
    |-module1env2/
    |    |-build.sbt
    |    |-project
    |    |      |-Dependencies/Versions/buid.properties
    |    |-src/
    |        |-main
    |            |-scala
    |              |-core
    |              |-envSpecificThings
```
Now build.sbt for env1 both env1 and env2 will have it's root defined like this:
```
lazy val wasp = (project in file("."))
    .aggregate(module1Ref)
    .dependsOn(module1Ref)
    .settings(
        Compile / scalaSource := baseDirectory.value / "../../src/main/scala"
    )
```
While the only difference will be external project they reference. For env1 that will be module1env1:
```
lazy val module1Ref = ProjectRef(file("../../module1env1"), "module1env1")
```
And for env2 that will be module1env2:
```
lazy val module1Ref = ProjectRef(file("../../module1env2"), "module1env2")
```

Now with this, we will have core of the module in two projects. That means we have code repetition, but we can eliminate it by creating another project called module1Core and putting source code of the core there. After that we can just add that code in unmanagedSourceDirectories in both modules, for every environment
.
Add this to build.sbt in environment specific module:
```
Compile / unmanagedSourceDirectories += baseDirectory.value / "../module1Core/src/main/scala"

librabryDependencies ++= Seq(
    // dependencies both for core and environment specific things should go here
)
```

Note that we could do something like this in moduleCore/build.sbt and then just reference Core, not modules separately:
```
lazy val module1 = (project in file("."))
    .settings(
        // add lib dependencies for core
    )
    .dependsOn(module1RefEnv1)
    .dependsOn(module1RefEnv2)

lazy val module1RefEnv1 = ProjectRef(file("../../module1env1"), "module1env1")
lazy val module1RefEnv2 = ProjectRef(file("../../module1env2"), "module1env2")
```
But then we would again need have some sort of switch inside because we would need to choose which environment to reference with ProjectRef (we don't want to reference both in the same build). Because of that we need to find other solutions.

Now we will have only one source for core, but dependencies are a problem, because both modules from both environments will have to build core which has it's own dependencies meaning that we will have to define core's dependencies in both modules for env1 and env2.

In order to organize project's file hierarchy better, we will put module1env[1, 2, etc.] and module's core into one directory. We end up with something like this for each module:
```
|solution-2
    |-builds/
    |-[env1, env2, etc.]
    |-module1/
        |-module1Core
        |    |-src/
        |       |-main/
        |           |-scala
        |               |-moduleCorePackages
        |-module1env1/
        |    |-build.sbt // <- define how would you build this (add sources from module1Core), end then just reference that in ../builds/env1/build.sbt
        |    |-project
        |    |   |-Dependencies/Versions/buid.properties
        |    |-src/
        |        |-main
        |            |-scala
        |               |-envSpecificPackages
        |
        |-module1env2/
        |    |-build.sbt
        |    |-project
        |    |   |-Dependencies/Versions/buid.properties
        |    |-src/
        |        |-main
        |            |-scala
        |               |-envSpecificPackages
```

Even after that improvement, this approach is still not good because there is a repetition of code (Dependencies for cores are defined in each of the environments). Another problem is that build.sbt files for environments are almost the same. The only difference is that they will reference different modules.

### Trying another alternative (solution-2-2)

There is another solution that we can try, and in it the project structure will look like this:
```
solution-2/
    |-builds/
    |    |-env1/
    |    |   |-build.sbt
    |    |   |-project/
    |    |   |   |-Dependencies
    |    |   |   |-build.properties
    |    |   |
    |    |   |-module1/
    |    |   |   |-src/main/
    |    |   |      |-scala <- core and environment specific code
    |    |   |
    |    |   |-module2/
    |    |   |   |-src/main/
    |    |   |      |-scala <- core and environment specific code
    |    |
    |    |-env2/
    |        |-build.sbt
    |        |-project/
    |        |   |-Dependencies
    |        |   |-build.properties
    |        |
    |        |-module1/
    |        |   |-src/main/
    |        |      |-scala <- core and environment specific code
    |        |
    |        |-module2/
    |        |   |-src/main/
    |        |      |-scala <- core and environment specific code
```

This approach might be better because we can build modules directly from builds/env/ and not reference them as we had to do in the previous example. Problem of code repetition remains, because dependencies for each core of the module will have to be included in every builds/env/project/Dependencies.scala file. Also, we will have source for module's core in both of the environments which is not good but that can be resolved by adding it to Compile / unmanagedSourceDirectories for each environment. To me, this file structure kinda makes more sense because everything one environment might need is inside of it, but this still isn't solution we are looking for (because of already mentioned code repetition). Other solutions might be better.
