import sbt._

object Dependencies {
    val sparkCore = "org.apache.spark" %% "spark-core" % Versions.spark
    val sparkSql = "org.apache.spark" %% "spark-sql" % Versions.spark
}