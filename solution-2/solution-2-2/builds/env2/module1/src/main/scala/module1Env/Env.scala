package module1Env

import org.apache.spark.sql.SparkSession

object Env {
    def getEnv: String = "environment 2"

    def getSparkVersion: String = {
        val spark = SparkSession
            .builder()
            .master("local")
            .getOrCreate()
        val ver = spark.version
        spark.close()
        ver
    }
}