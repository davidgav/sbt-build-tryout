name := "solution-2-2-env1"
version := "0.1"
scalaVersion := Versions.scalaVersion

lazy val wasp = (project in file("."))
    .settings(
        Compile / scalaSource := baseDirectory.value / "../../src/main/scala"
    )
    .aggregate(module1, module2)
    .dependsOn(module1)
    .dependsOn(module2)

lazy val module1 = (project in file("module1"))
    .settings(
        libraryDependencies ++= Seq(
            Dependencies.sparkCore,
            Dependencies.sparkSql
        )
    )

lazy val module2 = (project in file("module2"))
    .settings(
        // add dependencies
    )
